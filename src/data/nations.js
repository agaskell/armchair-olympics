export default {
	"nor": {
		"code": "nor",
		"fullName": "Norway",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Norway.svg/45px-Flag_of_Norway.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-Norway.png",
		"weight": 1
	},
	"can": {
		"code": "can",
		"fullName": "Canada",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/en/thumb/c/cf/Flag_of_Canada.svg/45px-Flag_of_Canada.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-Canada.png",
		"weight": 1
	},
	"usa": {
		"code": "usa",
		"fullName": "United States of America",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/en/thumb/a/a4/Flag_of_the_United_States.svg/45px-Flag_of_the_United_States.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-United-States-of-America.png",
		"weight": 1
	},
	"ger": {
		"code": "ger",
		"fullName": "Germany",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/en/thumb/b/ba/Flag_of_Germany.svg/45px-Flag_of_Germany.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-Germany.png",
		"weight": 2
	},
	"sui": {
		"code": "sui",
		"fullName": "Switzerland",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Switzerland.svg/45px-Flag_of_Switzerland.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-Switzerland.png",
		"weight": 3
	},
	"blr": {
		"code": "blr",
		"fullName": "Belarus",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/45px-Flag_of_Belarus.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-Belarus.png",
		"weight": 5
	},
	"aut": {
		"code": "aut",
		"fullName": "Austria",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Flag_of_Austria.svg/45px-Flag_of_Austria.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-Austria.png",
		"weight": 2
	},
	"fra": {
		"code": "fra",
		"fullName": "France",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/en/thumb/c/c3/Flag_of_France.svg/45px-Flag_of_France.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-France.png",
		"weight": 2
	},
	"chn": {
		"code": "chn",
		"fullName": "People's Republic of China",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_the_People%27s_Republic_of_China.svg/45px-Flag_of_the_People%27s_Republic_of_China.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-China.png",
		"weight": 3
	},
	"kor": {
		"code": "kor",
		"fullName": "Korea",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Unification_flag_of_Korea_%28pre_2006%29.svg/45px-Unification_flag_of_Korea_%28pre_2006%29.svg.png",
		"alternateFlagUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Unification_flag_of_Korea.svg/1200px-Unification_flag_of_Korea.svg.png",
		"weight": 4
	},
	"swe": {
		"code": "swe",
		"fullName": "Sweden",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/en/thumb/4/4c/Flag_of_Sweden.svg/45px-Flag_of_Sweden.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-Sweden.png",
		"weight": 2
	},
	"jpn": {
		"code": "jpn",
		"fullName": "Japan",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/en/thumb/9/9e/Flag_of_Japan.svg/45px-Flag_of_Japan.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-Japan.png",
		"weight": 4
	},
	"fin": {
		"code": "fin",
		"fullName": "Finland",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Flag_of_Finland.svg/45px-Flag_of_Finland.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-Finland.png",
		"weight": 6
	},
	"gbr": {
		"code": "gbr",
		"fullName": "Great Britain",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/45px-Flag_of_the_United_Kingdom.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png",
		"weight": 8
	},
	"ukr": {
		"code": "ukr",
		"fullName": "Ukraine",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Flag_of_Ukraine.svg/45px-Flag_of_Ukraine.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-Ukraine.png",
		"weight": 15
	},
	"ita": {
		"code": "ita",
		"fullName": "Italy",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/en/thumb/0/03/Flag_of_Italy.svg/45px-Flag_of_Italy.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-Italy.png",
		"weight": 4
	},
	"lat": {
		"code": "lat",
		"fullName": "Latvia",
		"flagUrl": "http://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Flag_of_Latvia.svg/45px-Flag_of_Latvia.svg.png",
		"alternateFlagUrl": "https://www.countries-ofthe-world.com/flags-normal/flag-of-Latvia.png",
		"weight": 8
	}
}
