import fetch from 'node-fetch'
import fs from 'fs';
import http from 'http'

import cache from './cache'
import countriesByPerson from './data/people'
import nations from './data/nations'

const getMedals = async () => {
	if (cache.result) {
		return cache.result;
	}

	const response = await fetch('http://data.cnn.com/olympics/2018/results/country.json')
	const { countries, timestamp } = await response.json()
	const medals = countries.reduce((obj, {
		country,
		gold,
		silver,
		bronze
	}) => {
		obj[country.toLowerCase()] = gold + silver + bronze
		return obj
	}, {})
	const result = { lastUpdated: timestamp, medals }
	cache.result = result
	setTimeout(() => delete cache.result, 1000 * 60 * 30)
	return result
}

const getStandings = async () => {
  const { lastUpdated, medals } = await getMedals();
  
	const pointsPerPerson = Object.keys(countriesByPerson).reduce((points, person) => {
		points[person] = countriesByPerson[person].reduce((total, country) =>
			total + (medals[country] * nations[country].weight), 0)
		return points
	}, {})

	const standings = Object.keys(countriesByPerson)
		.sort((a, b) =>
			(pointsPerPerson[b] - pointsPerPerson[a]) || a.localeCompare(b))
		.map(person => ({
			person,
			score: pointsPerPerson[person],
			entries: countriesByPerson[person].sort().map(country => nations[country])
		}));

	const rankingByWeight = Object.keys(nations).reduce((rankings, country) => {
		rankings.push({'name': [country][0],
			'point': medals[country] * nations[country].weight,
			'flagUrl': nations[country].flagUrl,
			'weight': nations[country].weight,
			'medalCount': medals[country]
		})
		return rankings;
	}, []).sort((a,b) => b.point - a.point);

	rankingByWeight.reduce((rank, country, index, rankings) => {
		country.rank = rank;
		return country.point === (rankings[index + 1] || {}).point ?
			rank :
			rank + 1
	}, 1);

	const maxPoint = rankingByWeight.reduce((max, country, index) => {
		index < 3 && (max += country.point);
		return max;
	}, 0);

	standings.reduce((rank, standing, index, standings) => {
		standing.rank = rank;
		return standing.score === (standings[index + 1] || {}).score ?
			rank :
			rank + 1
	}, 1);

	return { lastUpdated, standings, rankingByWeight, maxPoint }
};

const staticHtml = fs.readFileSync('./index.html');
const staticJs = fs.readFileSync('./client.js');
const staticCss = fs.readFileSync('./client.css');

const router = async (req, res) => {
	const map = {
		'/client.js': {
			head: { 'Content-Type': 'application/javascript' },
			data: () => staticJs
		},
		'/client.css': {
			head: { 'Content-Type': 'text/css' },
			data: () => staticCss
		},
		'/standings': {
			head: {
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*'
			},
			data: async () => JSON.stringify(await getStandings())
		},
		'/medals': {
			head: {
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*'
			},
			data: async () => JSON.stringify(await getMedals())
		}
	}

	const response = map[req.url] || {
		head: { 'Content-Type': 'text/html' },
		data: () => staticHtml
	}

	res.writeHead(200, response.head)
	res.end(await response.data())
}


http
	.createServer(router)
	.listen(process.env.PORT || 8080)
